// Default URL for triggering event grid function in the local environment.
// http://localhost:7071/runtime/webhooks/EventGrid?functionName={functionname}
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.IO;
using Microsoft.Azure.Storage.Blob;
using SixLabors.ImageSharp;
using System;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Formats;
using System.Text.RegularExpressions;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Formats.Gif;
using System.Threading.Tasks;

namespace FunctionThumbnails
{
    public static class FunctionThumbnails
    {
        [FunctionName("FunctionThumbnails")]
        public static async Task Run(
            [EventGridTrigger] EventGridEvent eventGridEvent,
            [Blob("{data.url}", FileAccess.Read, Connection = "ConnectionString")] CloudBlockBlob file,
            ILogger log)
        {
            log.LogInformation($"File: {file.Name}");

            var thumbnailDirectoryName = Environment.GetEnvironmentVariable("ThumbnailDirectoryName");
            var parentDirectoryName = GetLastSection(file.Parent.Prefix);
            
            if(parentDirectoryName == thumbnailDirectoryName)
            {
                log.LogInformation("Ignored thumbnails upload");
                return;
            }

            var extension = file.Name.Split(".").Last();
            var encoder = GetImageEncoder(extension);
            
            if(encoder != null)
            {
                var thumbnailsDirectory =  file.Parent.GetDirectoryReference(thumbnailDirectoryName);
                var thumbnail = thumbnailsDirectory.GetBlockBlobReference(GetLastSection(file.Name));

                
                using (var resizedImage = new MemoryStream())
                using (var image = Image.Load(file.OpenRead()))
                {

                    var thumbnailSize = CalculateThumbnailSize(image.Size(), log);
                    image.Mutate(x => x.Resize(thumbnailSize));
                    image.Save(resizedImage, encoder);

                    resizedImage.Position = 0;
                    await thumbnail.UploadFromStreamAsync(resizedImage, resizedImage.Length);

                    log.LogInformation($"Original size: {file.Properties.Length} thumbnail size: {thumbnail.Properties.Length}");
                }

            }
            else
            {
                log.LogInformation("No encoder found for this file format");
            }
            
        }

        private static Size CalculateThumbnailSize(Size imageSize, ILogger log)
        {
            var thumbnailMaxSize = Convert.ToDecimal(Environment.GetEnvironmentVariable("ThumbnailMaxSize"));

            var scale = Math.Min(decimal.Divide(thumbnailMaxSize,imageSize.Width), decimal.Divide(thumbnailMaxSize, imageSize.Height));
            log.LogInformation(scale.ToString() + " " + imageSize.Width + " " + imageSize.Height);
            return new Size()
            {
                Width = Convert.ToInt32( Math.Round(imageSize.Width * scale) ),
                Height = Convert.ToInt32( Math.Round(imageSize.Height * scale) )
            };
        }

        private static IImageEncoder GetImageEncoder(string extension)
        {
            IImageEncoder encoder = null;

            var isSupported = Regex.IsMatch(extension, "gif|png|jpe?g", RegexOptions.IgnoreCase);

            if (isSupported)
            {
                switch (extension.ToLower())
                {
                    case "png":
                        encoder = new PngEncoder();
                        break;
                    case "jpg":
                        encoder = new JpegEncoder();
                        break;
                    case "jpeg":
                        encoder = new JpegEncoder();
                        break;
                    case "gif":
                        encoder = new GifEncoder();
                        break;
                    default:
                        break;
                }
            }
            return encoder;
        }

        private static string GetLastSection(string url)
        {
            if(url.Last() == '/')
            {
                url = url.Substring(0, url.Length - 1);
            }
            return url.Split("/").Last();
        }
    }
}
